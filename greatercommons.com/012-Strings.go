/**
 * Start learn golang
 * Section4 - cap4 Strings Type
 * Date: Sat Aug 11 23:35:11 -03 2018
 */
package main

import "fmt"

func main() {
	sTabs := `Hello 
			
Playground
`

	sSpaces := `Hello 
            
Playground
`
	numberOfBytes, errors := fmt.Printf("%v ->%T\n", sTabs, sTabs)
	fmt.Println(numberOfBytes, errors)
	fmt.Printf("%v %T\n", sTabs, sTabs)

	numberOfBytes, errors = fmt.Printf("%v -> %T\n", sSpaces, sSpaces)
	fmt.Println(numberOfBytes, errors)

	//s := "Hello Playground, here ção ¶•ªº£文字香"
	s := "ção ¶•ªº£文字香"
	fmt.Printf("%v ->%T\n", s, s)

	sb := []byte(s)
	fmt.Printf("%v ->%T\n", sb, sb)

	for _, v := range sb {
		fmt.Printf("b:%b\t v:%d\t %U - %#x\t char:%c\n", v, v, v, v, v)

	}
	fmt.Println("--------------------------------")
	for _, v := range s {
		fmt.Printf("b:%b\t v:%d\t %U - %#x\t char:%c\n", v, v, v, v, v)

	}
	fmt.Println("--------------------------------")
	for i := 0; i < len(s); i++ {
		fmt.Printf("b:%b\t v:%d\t %U - %#x\t char:%c\n", s[i], s[i], s[i], s[i],s[i])
	}

}
