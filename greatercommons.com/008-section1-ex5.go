/**
 * Start learn golang
 * Section3 - Exercise 5
 * Date: Sat Aug 11 21:49:04 -03 2018
 */
package main

import "fmt"

type myType int

var x myType
var y int

func main() {
	fmt.Println("Valor de x", x)
	x = 42
	fmt.Printf("valor de x:%d\t tipo:%T\n", x, x)
	y = int(x)

	fmt.Printf("valor de y:%d\t tipo:%T\n", y, y)

}
