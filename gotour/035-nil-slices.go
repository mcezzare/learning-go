package main

import "fmt"

func main() {
	var s []int
	printSlice(s)

	if s == nil {
		fmt.Println("saporra is nil")
	}
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v em %p\n", len(s), cap(s), s, s)
}
