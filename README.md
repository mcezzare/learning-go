# learning-go

My scripts written while learning GoLang

## Folders

Folder greatercommons.com : following this course : https://greatercommons.com/learn/golang-ptbr

Folder book-1: following this book : (Programando em Go
Crie aplicações com a linguagem do Google) https://www.casadocodigo.com.br/products/livro-google-go (in my Gdrive)

Folder gotour : Following the gotour app offline or [online english](https://tour.golang.org/welcome/1) or in [online pt-br](https://go-tour-br.appspot.com/)

I created a package to dump slices to avoid write a function in every file : https://gitlab.com/mcezzare/go-packages drop this file in a package named  sliceutil