/**
 * Start learn golang
 * Operators
 * Date: Fri Aug 10 23:20:37 -03 2018
 */
package main

import "fmt"

var z = 45
func main(){

	x:= 10 + 10
	fmt.Printf("valor de x: %v\t tipo: %T\n" , x, x)
	qualqueroisa(x)


	test := 10 == 10
	fmt.Printf("valor de test: %v\t tipo: %T\n" , test, test)

	test = 10 != 10
	fmt.Printf("valor de test: %v\t tipo: %T\n" , test, test)

	//cannot use test (type bool) as type int in argument to qualqueroisa
	//qualqueroisa(test)
	qualqueroisa(z)
}


func qualqueroisa(x int){
	fmt.Println(x);
	//hold package level scope
	fmt.Println(z);
}