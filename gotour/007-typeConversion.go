package main

import (
	"math"
	"fmt"
)

func main(){

	var x, y int = 3, 4
	var f float64 = math.Sqrt(float64(x*x +y*y))

	// uncomment line 14 ands comment line 15 and run
	//z:=f
	var z uint	= uint(f)

	fmt.Printf("%v:\t%T\n", x, x)
	fmt.Printf("%v:\t%T\n", f, f)
	fmt.Printf("%v:\t%T\n", z, z)

	}