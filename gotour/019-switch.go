package main

import (
	"fmt"
	"runtime"
)

func main(){
	fmt.Print("Go runs on ")
	switch os:=runtime.GOOS;os {
	case "darwin":
		fmt.Println("OS X")
	case "linux":
		fmt.Println("Linux")
	default:
		fmt.Printf("%s", os)
	}

	fmt.Printf("Version: %s\n", runtime.Version())
	fmt.Printf("Platform: %s\n" , runtime.GOARCH)
}