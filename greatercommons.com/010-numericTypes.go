/**
 * Start learn golang
 * Section4 - cap3 numeric types - https://golang.org/ref/spec#Numeric_types
 * Date: Sat Aug 11 22:41:16 -03 2018
 */
package main

import (
	"fmt"
	"runtime"
)

var x bool

func main(){
	a := "e"
	b := "é"
	c := "•"

	fmt.Printf("%v,%v,%v\n" , a, b, c)

	d:=[]byte(a)
	e:=[]byte(b)
	f:=[]byte(c)

	fmt.Printf("%v,%v,%v\n" , d, e, f)


	x:= 10
	y:= 10.0

	fmt.Printf("%v tipo:%T,\n" , x, x)
	fmt.Printf("%v tipo:%T,\n" , y, y)

	fmt.Println(runtime.GOOS)
	fmt.Println(runtime.GOARCH)


}