package main

import (
	"fmt"
	"github.com/mcezzare/sliceutil"

)

func main(){

	a := make([]int , 5)
	printSlice("a", a)
	sliceutil.ShowSliceOfInt("a", a)

	b:= make([]int ,0 ,5)
	printSlice("b", b)
	sliceutil.ShowSliceOfInt("b", b)

	c:= b[:2]
	printSlice("c", c)
	sliceutil.ShowSliceOfInt("c", c)


}
// code migrated to function ShowSlice in
func printSlice(s string , x []int) {
	fmt.Printf("%s len=%d cap=%d %v com #%v em %p\n", s, len(x), cap(x), x, x, x)
}