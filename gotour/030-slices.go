package main

import "fmt"

func main(){
	primes:=[6]int{2,3,5,7,11,13}

	var s []int = primes[1:4]
	fmt.Printf("valor de s: %v  %T\n", s, s)

	var s2 []int = primes[1:2]
	fmt.Printf("valor de s2: %v  %T\n", s2, s2)

	var s3 []int = primes[2:3]
	fmt.Printf("valor de s3: %v  %T\n", s3, s3)


	for i:=0;i<len(primes);i++{
		fmt.Printf("valores de primes usando %d:%d ,%v\n" , 0 ,i+1 , primes[0:i+1])
		fmt.Printf("valores de primes usando %d:%d ,%v\n" , i ,i+1 , primes[i:i+1])
		//fmt.Printf("valores de primes usando %d:%d ,%v\n" , i ,i+2 , primes[i:i+2])
	}


}