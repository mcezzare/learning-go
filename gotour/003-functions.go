/*
https://blog.golang.org/gos-declaration-syntax
*/

package main

import (
	"fmt"
)

func add(x, y int) int {
	return x + y
}

func add2( x int, y int) int{
	return x + y
}

func main() {
	fmt.Println(add(55, 113))
	fmt.Println(add2(55, 113))
}
