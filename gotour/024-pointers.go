package main

import "fmt"

func main(){
	i,j:= 42, 2701
	fmt.Printf("valor de i:%v , %T\n",  i, i )
	p:= &i			// point to i
	fmt.Printf("valor de p:%v , %T\n",  p, p )
	fmt.Printf("valor de *p:%v , %T\n",  *p, *p ) // read i through the pointer
	*p=21;			// set i through the pointer
	fmt.Printf("valor de *p:%v , %T\n",  *p, *p )
	fmt.Println(i)	// see the new value of i
	p= &j			// point to j
	*p = *p/37		// divide j through the pointer
	fmt.Println(j)	// see the new value of j
}