/**
 * Start learn golang
 * Hello World example, pos undestanding how to setup the workspace.
 * Date: Fri Aug 10 21:18:19 -03 2018
 */
package main

import "fmt"

func main() {
	fmt.Println("Hello Dude")
	// dump returning values
	// function Prinln return 2 values, it's mandatory
	numberOfBytes, errors := fmt.Println("Hello Dude again")
	fmt.Println(numberOfBytes, errors)

	// if variable will not be used, use "_" in position
	//@see https://golang.org/ref/spec#Blank_identifier
	_, errors2 := fmt.Println("Hello Dude over again")
	fmt.Println(errors2)

}
