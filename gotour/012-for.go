package main

//Essa é pegadinha

import "fmt"

func main() {
	sum := 1
	fmt.Println("Valor inicial de sum:\t", sum)
	for ; sum < 100; {
		fmt.Println("Valor de sum antes:\t\t", sum)
		sum += sum
		fmt.Println("Valor de sum depois:\t", sum)
	}

	fmt.Println("Valor final de sum:\t", sum)

	var i int
	fmt.Println("Valor inicial de i:\t", i)
	for i = 1; i < 100; i++ {
		fmt.Println("Valor de i antes:\t", i)
		i += i
		fmt.Println("Valor de i depois:\t", i)
	}

	fmt.Println("Valor final de i:\t", i)
}
