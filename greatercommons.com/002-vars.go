/**
 * Start learn golang
 * Variables
 * Date: Fri Aug 10 22:04:19 -03 2018
 */
package main

import "fmt"

var packageLevelScope = 37
// syntax error: non-declaration statement outside function body
//packageLevelScope := 37

func main(){
	/**
	 *	:= Declaring vars
	 *  valid only inside scope
	 */
	x := 16
	w := 5.4
	y := "string value"
	z := true

	//fmt.Println(x,y,z)
	fmt.Printf("x: %v , %T\n" , x, x)
	fmt.Printf("w: %v , %T\n" , w, w)
	fmt.Printf("y: %v , %T\n" , y, y)
	fmt.Printf("z: %v , %T\n" , z, z)

	// Assign operator
	x = 20
	fmt.Printf("x: %v , %T\n" , x, x)

	x, k := 45, 10.2
	fmt.Printf("x: %v , %T\n" , x, x)
	fmt.Printf("k: %v , %T\n" , k, k)

	fmt.Printf("packageLevelScope: %v , %T\n" , packageLevelScope, packageLevelScope)



}