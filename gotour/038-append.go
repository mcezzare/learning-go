package main

import "github.com/mcezzare/sliceutil"

func main(){

	var s [] int
	sliceutil.ShowSliceOfInt("s", s)

	s = append(s,0)
	sliceutil.ShowSliceOfInt("s", s)

	s = append(s,1)
	sliceutil.ShowSliceOfInt("s", s)

	s = append(s,2,3,4)
	sliceutil.ShowSliceOfInt("s", s)
}