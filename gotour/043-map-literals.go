package main

import "fmt"

type CompanyLocations struct {
	Lat, Long float64
}

var m = map[string]CompanyLocations{
	"Bell Labs": CompanyLocations{
		40.68433, -74.39967,
	},
	"Google": CompanyLocations{
		37.42202, -122.08408,
	},
}

func main() {
	fmt.Printf("Map: %v %T em %p \n ", m, m, m)
}
