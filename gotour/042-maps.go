package main

import "fmt"

type MyType struct {
	Lat, Long float64
}

var m map[string]MyType

func main() {
	fmt.Printf("Map: %v %T em %p \n ", m, m, m)
	m = make(map[string]MyType)

	m["Bells"] = MyType{
		40.68433, -74.39967,
	}

	fmt.Printf("Map: %v %T em %p \n ", m, m, m)
	fmt.Println(m["Bells"])

}
