package main

import (
	"strings"
	"fmt"
	"golang.org/x/tour/wc"
)

func WordCount(s string) map[string] int{
	//return map[string]int{"x":1}
	m:= make(map[string]int)
	DumpMap(m)
	a:=strings.Fields(s)
	for _,v:= range a{
		m[v]++
		fmt.Println("valor de v ", v )

	}
	return m
}

func DumpMap(m map[string]int) {
	fmt.Printf("Map: %v %T em %p \n ", m, m, m)
}

func main(){
	//s:= "I am learning Go"
	//fmt.Printf("Fields are: %q\n" , strings.Fields(s) )
	wc.Test(WordCount)

}