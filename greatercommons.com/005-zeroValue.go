/**
 * Start learn golang
 * Zero Values
 * Date: Sat Aug 11 00:33:26 -03 2018
 */

package main

import "fmt"

var a int
var b float64
var c string
var d bool

func main(){

	fmt.Printf("%v, %T\n" , a , a)
	fmt.Printf("%v, %T\n" , b , b)
	fmt.Printf("%v, %T\n" , c , c)
	fmt.Printf("%v, %T\n" , d , d)
}