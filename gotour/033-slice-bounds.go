package main

import "fmt"

func main() {
	s := []int{2, 3, 5, 7, 11, 13}

	s = s[1:4]
	fmt.Println(s)

	s = s[:2]
	fmt.Println(s)

	s = s[1:]
	fmt.Println(s)

	s2 := []int{2, 3, 5, 7, 11, 13}
	fmt.Println(len(s2))
	fmt.Println(s2)
	fmt.Println(s2[0:6])
	fmt.Println(s2[:6])
	fmt.Println(s2[0:])
	fmt.Println(s2[:])




}
