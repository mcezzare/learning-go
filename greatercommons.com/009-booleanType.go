/**
 * Start learn golang
 * Section4 - boolean type
 * Date: Sat Aug 11 22:19:01 -03 2018
 */
package main

import "fmt"

var x bool

func main(){
	fmt.Println(x)

	x = (x==false)
	fmt.Println(x)
	x = true
	fmt.Println(x)

	x = (10<100)
	fmt.Println(x)
	x = (10==100)
	fmt.Println(x)
	x = (10>100)
	fmt.Println(x)

}