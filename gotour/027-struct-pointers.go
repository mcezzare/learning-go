package main

import "fmt"

type Vertex struct {
	X int
	Y int
}

func main(){
	v:= Vertex{1,2}
	p:= &v
	p.X = 1e9
	fmt.Printf("valor de v: %v , %T\n",  v, v )
	fmt.Printf("valor de p: %v , %T\n",  p, p )
}

