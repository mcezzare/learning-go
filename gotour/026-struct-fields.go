package main


import "fmt"

type MyType struct {
	X int
	Y int
}

func main(){
	v:= MyType{1,2}
	v.X = 4
	fmt.Printf("#%v , %T\n" , v.X, v.X)
	fmt.Printf("#%v , %T" , v, v)
}