package main

import "fmt"

func main() {
	m := make(map[string]int)
	DumpMap(m)

	m["Answer"] = 42
	DumpMap(m)
	fmt.Printf("The value is %d\n", m["Answer"])

	m["Answer"] = 48
	DumpMap(m)
	fmt.Printf("The value is %d\n", m["Answer"])

	delete(m, "Answer")
	DumpMap(m)
	fmt.Printf("The value is %d\n", m["Answer"])

	v, ok := m["Answer"]
	fmt.Printf("The value: %d  with type %T is Present: %t , %T", v, v, ok, ok)

}

func DumpMap(m map[string]int) {
	fmt.Printf("Map: %v %T em %p \n ", m, m, m)
}
