/**
 * Start learn golang
 * Section3 - Exercise 3
 * Date: Sat Aug 11 21:42:16 -03 2018
 */
package main

import "fmt"

func main() {
	x := 42
	y := "James Bond"
	z := true

	//fmt.Printf("valores de x:%d\t y:%s\t z:%s\t", x, y, z)
	s := fmt.Sprintf("%v%v%v", x, y, z)
	fmt.Println(s)

}
