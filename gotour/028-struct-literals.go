package main

import "fmt"

type Vertex struct {
	X, Y int
}

var (
	v1 = Vertex{1, 2}  	// has type Vertex
	v2 = Vertex{X: 1}  			// Y:0 is implicit
	v3 = Vertex{}      			// X:0 and Y:0
	p  = &Vertex{1, 2} 	// has type *Vertex
)

func main() {
	fmt.Println(v1, p, v2, v3)

	fmt.Printf("valor de v1: %v , %T\n", v1, v1)
	fmt.Printf("valor de v2: %v , %T\n", v2, v2)
	fmt.Printf("valor de v3: %v , %T\n", v3, v3)
	fmt.Printf("valor de p: %v , %T\n", p, p)
}
