package main

import "fmt"

func fibonacci() func() int {
	llast, last := 0, 1

	return func() int {
		llast, last = last, last + llast

		return last
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
