package main

import "fmt"

const immutable  = "This value never change"
const Immutable  = "This value never change and is public"
const Pi = 3.14


func PublicDumpConst(){
	fmt.Println(Immutable)
}


func main(){
	PublicDumpConst()

	const World = "世界"
	fmt.Println("Hello", World)
	fmt.Println("Happy", Pi, "Day")

	const Truth = true
	fmt.Println("Go rules?", Truth)
}
