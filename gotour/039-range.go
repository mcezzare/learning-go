package main

import (
	"fmt"
	"github.com/mcezzare/sliceutil"
)


var pow = []int {1,2,4,8,16,32,64,128}
var words = []string{"a", "b", "c", "d", "ola"}
func main(){
	for i,v := range pow{
		fmt.Printf("2**%d = %d\n" , i , v )
	}
	sliceutil.ShowSliceOfInt("pow" , pow)
	fmt.Println("------")
	for i,v:= range words{
		fmt.Printf("index:%d\t value:%s\n", i , v)
	}
	sliceutil.ShowSliceOfStrings("words" , words)
}
