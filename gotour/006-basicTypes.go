package main

import (
	"math/cmplx"
	"fmt"
)

var(
	ToBe bool	= false
	MaxInt	uint64	= 1<<64-1
	z complex128 = cmplx.Sqrt(-5+12i)
)

func main(){

	fmt.Printf("Valor: %v\t Tipo:%T\n" , ToBe , ToBe)
	fmt.Printf("Valor: %v\t Tipo:%T\n" , MaxInt , MaxInt)
	fmt.Printf("Valor: %v\t Tipo:%T\n" , z , z)
}