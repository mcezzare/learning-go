package main

/**
Implemente Pic. Ela deve retornar uma slice de comprimento dy, cada elemento do qual
é uma slice de dx 8-bit inteiros sem sinal. Quando você executa o programa, ele irá exibir
a sua imagem, interpretando os números inteiros como escala dos valores de cinza (bem, bluescale).

A escolha da imagem é com você. Funções interessantes incluem x^y, (x+y)/2, e x*y.

(Você precisa usar um loop para alocar cada []uint8 dentro do [][]uint8.)

(Utilize uint8(intValue) para converter entre os tipos.)

check the result at : https://codebeautify.org/base64-to-image-converter
good examples at : https://gist.github.com/tetsuok/2280162
 */



import (
	"github.com/mcezzare/sliceutil"
	"fmt"
	"golang.org/x/tour/pic"
)

var array = []int{2, 4, 6, 7, 8, 9}
var s = array[0:5]
var final = make([][]uint8, len(array))

func Pic(dx, dy int) [][]uint8 {
	ret := make([][]uint8, dy)

	for i := 0; i < dy ; i++ {
		ret[i]=make([]uint8 ,dx)
	}

	for i := 0; i < dy; i++ {
		for j := 0; j < dx; j++ {
			switch {
			case j % 15 == 0:
				ret[i][j] = 80
			case j % 3 == 0:
				ret[i][j] = 140
			case j % 5 == 0:
				ret[i][j] = 150
			default:
				ret[i][j] = 80
			}
		}
	}
	sliceutil.ShowSliceOfUInt8("ret",ret)
	return ret
}

func Pic2(dx, dy int) [][]uint8 {

	mypic := make([][]uint8, dy)
	for i := 0; i < dy; i++ {
		mypic[i] = make([]uint8, dx)
		for j := 0; j < dx; j++ {
			mypic[i][j] = uint8((j-i)^i-j)
		}
	}
	return mypic
}

func main() {
	x:= 10
	y:= 5
	fmt.Printf("^ %d \n" , x^y)
	fmt.Printf("x+y/2 %.2f \n" , float64((x+y)/2))
	sliceutil.ShowSliceOfInt("array",array)
	sliceutil.ShowSliceOfInt("s",s)
	sliceutil.ShowSliceOfUInt8("final",final)
	pic.Show(Pic)
	pic.Show(Pic2)
}
