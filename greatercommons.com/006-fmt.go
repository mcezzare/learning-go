/**
 * Start learn golang
 * Fmt
 * Date: Sat Aug 11 00:44:34 -03 2018
 */

package main

import "fmt"

type point struct {
	x, y int
}

func main(){
	x:= "oi"
	y:= "bom dia"
	// ad spaces between
	fmt.Println(x,y)

	// don add spaces
	z:= fmt.Sprint(x,y)
	fmt.Println(z)

	p:= point{1,2}
	fmt.Printf("%v \n", p)
	//Go syntax representation of the value
	fmt.Printf("%#v\n", p)
	// Types
	fmt.Printf("%T\n", p)
	fmt.Printf("%v => %T\n", x, x)

	// assign return to value
	numberOfBytes, errors := fmt.Println(z)
	fmt.Println(numberOfBytes, errors)

	fmt.Printf("%t\n", true)
	// decimal
	fmt.Printf("%d\n", 123)
	// binary
	fmt.Printf("%b\n", 123)
	// chars of asc
	fmt.Printf("%c\n", 33)

	fmt.Printf("%x\n", 10)

	fmt.Printf("%q\n", true)
	fmt.Printf("%q\n", z)


	//intervalo :=[1 : 255]
	//for i,n := range intervalo{
	//	fmt.Printf("%c\n", i)
	//}
	//

}