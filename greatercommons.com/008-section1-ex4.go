/**
 * Start learn golang
 * Section3 - Exercise 4
 * Date: Sat Aug 11 21:42:16 -03 2018
 */
package main

import "fmt"

type myType int

var x myType

func main() {
	fmt.Println("Valor de x", x)
	x := 42

	fmt.Printf("valor de x:%d\t tipo:%T\n", x, x)

}
