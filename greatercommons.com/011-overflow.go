/**
 * Start learn golang
 * Section4 - cap4 overflow
 * Date: Sat Aug 11 23:25:11 -03 2018
 */
package main

import "fmt"

func main(){
	var i uint16
	i = 65535
	fmt.Println(i)
	i = i+1
	fmt.Println(i)
	i = i+1
	fmt.Println(i)

	// constant 65536 overflows uint16
	//i = 65536
	//fmt.Println(i)
}