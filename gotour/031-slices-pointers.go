package main

import "fmt"

func main() {
	names := [4]string{
		"John",
		"Paul",
		"George",
		"Ringo",
	}
	fmt.Println(names)
	fmt.Printf("valor de names: %v  %T\n", names, names)

	a := names[0:2]
	b := names[1:3]
	fmt.Println(a, b)
	fmt.Printf("valor de a: %v  %T\n", a, a)
	fmt.Printf("len de a: %v  %p\n", len(a), a)

	b[0] = "XXX"
	fmt.Println(a, b)
	fmt.Printf("valor de b: %v  %T\n", b, b)
	fmt.Printf("valor de b: %v  %p\n", b, b)
	fmt.Println(names)
	fmt.Printf("valor de names: %v  %T\n", names, names)

}
