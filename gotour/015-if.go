package main

//https://go-tour-br.appspot.com/flowcontrol/5

import (
	"fmt"
	"math"
)

func sqrt(x float64) string {
	if x < 0 {
		return sqrt(-x) + "i"
	}
	return fmt.Sprint(math.Sqrt(x))
}

func main() {
	fmt.Println(sqrt(2), sqrt(-4))

	var s = 10
	a := s - 1
	if a < 10 {
		fmt.Printf("%v é < que %v", a, s)
	}

}
