/**
 * Start learn golang
 * Types
 * Date: Sat Aug 11 00:10:25 -03 2018
 */
package main

import "fmt"

func main(){
	x   := 16
	w   := 5.4
	h   := 0X16
	b16	:= 1E4
	y   := "string value"
	z   := true

	fmt.Printf("x: %v , %T\n" , x, x)
	fmt.Printf("w: %v , %T\n" , w, w)
	fmt.Printf("h: %v , %T\t %U \n" , h, h, h)
	fmt.Printf("b16: %v , %T %X \n" , b16, b16 , b16)
	fmt.Printf("b16: %v , %T %e \n" , b16, b16 , b16)
	fmt.Printf("y: %v , %T\n" , y, y)
	fmt.Printf("z: %v , %T\n" , z, z)




}