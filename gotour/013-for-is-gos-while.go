package main

import "fmt"

func main() {
	sum := 1
	fmt.Println("Valor inicial de sum:\t", sum)
	for sum < 100 {
		fmt.Println("Valor de sum antes:\t\t", sum)
		sum += sum
		fmt.Println("Valor de sum depois:\t", sum)
	}
	fmt.Println("Valor final de sum:\t", sum)
}
