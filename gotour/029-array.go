package main

import "fmt"

func main(){
	var a [2]string
	a[0] = "Hello"
	a[1] = "World"
	fmt.Println(a[0], a[1])
	fmt.Println(a)
	fmt.Printf("valor de a[0]: %v , %T\n", a[0], a[0])
	fmt.Printf("valor de a: %v , %T\n", a, a)

	primes := [6]int{2,3,5,7,11,13}
	fmt.Println(primes)
	fmt.Printf("valor de primes: %v  %T\n", primes, primes)
}