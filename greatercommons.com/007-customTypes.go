/**
 * Start learn golang
 * custom types
 * Date: Sat Aug 11 20:58:59 -03 2018
 */
package main

import "fmt"

type hotdog int
var s hotdog

func main(){
	x:= 10
	fmt.Println(s)
	fmt.Printf("%v -> %T\n" , s , s )
	fmt.Printf("%v -> %T\n" , x , x )

	//cannot use x (type int) as type hotdog in assignment
	//s= x

	// but it can be converted - https://golang.org/ref/spec#Conversions
	x = int(s)
	fmt.Printf("%v -> %T\n" , x , x )
}