/*
* Examples of usage of this conversor
* - go run cap2-conversor/conversor.go 32 27.4 -3 0 celsius
* - go run cap2-conversor/conversor.go 32 27.4 5 0 quilometros
* Date: Sun Aug 12 23:41:38 -03 2018
 */
package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) < 3 {
		fmt.Println("Uso: conversor <valores> <unidade>")
		os.Exit(1)
	}

	unidadeOrigem := os.Args[len(os.Args)-1]
	valoresOrigem := os.Args[1 : len(os.Args)-1]

	fmt.Printf("unidadeOrigem: %v -> %T\n", unidadeOrigem, unidadeOrigem)
	fmt.Printf("valoresOrigem: %v -> %T\n", valoresOrigem, valoresOrigem)

	var unidadeDestino string

	if unidadeOrigem == "celsius" {
		unidadeDestino = "fahrenheit"
	} else if unidadeOrigem == "quilometros" {
		unidadeDestino = "milhas"
	} else {
		fmt.Printf("%s não é uma unidade conhecida!", unidadeDestino)
		os.Exit(1)
	}

	for i, v := range valoresOrigem {
		valorOrigem, err := strconv.ParseFloat(v, 64)
		// _, err := strconv.ParseFloat(v, 64)
		if err != nil {
			fmt.Printf(
				"O valor %s na posição %d "+
					"não é um número válido!\n",
				v, i+1) // the position visible to user input
			os.Exit(2)
		}

		var valorDestino float64

		if unidadeOrigem == "celsius" {
			valorDestino = valorOrigem*1.8 + 32
		} else {
			valorDestino = valorOrigem / 1.60934
		}

		fmt.Printf("%.2f %s = %.2f %s\n",
			valorOrigem, unidadeOrigem,
			valorDestino, unidadeDestino)
	}
}
