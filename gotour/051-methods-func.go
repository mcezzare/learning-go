
package main

import (
	"math"
	"fmt"
)

type MyTypeWithMethods struct {
	X, Y float64
}

func Abs(v MyTypeWithMethods) float64{
	return math.Sqrt(v.X*v.Y+ v.Y*v.Y)
}

func main(){
	v:= MyTypeWithMethods{3,4}
	x:= Abs(v)
	fmt.Printf("%.2f with type %T at %p\n" , Abs(v), Abs(v), Abs(v))
	fmt.Printf("%.2f with type %T at %p" ,x, x, x)
}