package main

import "fmt"

func main() {
	sum := 0
	fmt.Println("Valor de sum:" , sum)
	for i := 0; i < 10; i++ {

		sum += i
		fmt.Println("Valor de sum:" , sum)
	}

	fmt.Println("Valor final de sum:" , sum)

}
